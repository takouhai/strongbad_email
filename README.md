# strongbad_email 🦀

> strong bad underscore email dot exe.
>
> enter.
>
> _- Strong Bad, [sbemail #68: caper](https://www.youtube.com/watch?v=HBUP2jAPExU)_

A Rust based CLI app that prints a random [Strong Bad Email](https://homestarrunner.com/sbemails), written in blazingly fast powerful Rust. 🦀

## Features

* Written in **Rust**
* Blazingly fast
* Uses the finest Rust libraries to decode the TOML file containing the sbemails
* Runs on your 386*

(*not sure if it actually runs on a 386, probably not)

## Install

You will need rustc/cargo installed. Then, install the package from Codeberg:

```
cargo install strongbad_email --git https://codeberg.org/takouhai/strongbad_email.git
```

then just run

```
strongbad_email
```
and you should be good to go.

## License

All of the text from the Strong Bad Emails are from the [Homestar Runner Wiki](http://www.hrwiki.org).

All of the Strong Bad Emails are from [Homestar Runner](https://homestarrunner.com), All Rights Reserved.

This program is licensed under the [MIT License](LICENSE).
